package com.project.appnomic;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class DataBreakage {
    public static void main(String[] args) throws IOException {
        System.out.println("Entering main() ");
        //String transactionDir = args[0];
        String allDir = args[0];
        String mstFile = args[1];
        String ConfigFile = args[2];
        //String transactionDestFile = args[4];
        String DestFile = args[3];
        String kpi_idLocation = args[4];
        if (mstFile.equals("null")){
            System.out.println("Master kpi details file not present. If possible, Please place it manually and give path as 3rd argument");
            System.exit(1);
        }
        if (ConfigFile.equals("null")){
            System.out.println("Config details file not present. If possible, Please place it manually and give path as 4th argument");
            System.exit(1);
        }

            List<String[]> records = allData.generateBreakageData(allDir,ConfigFile,mstFile,kpi_idLocation);
            prepareWriteDataFile(records,DestFile, ConfigFile);

//        List<String[]> records = allData.generateBreakageData(kpiDir,ConfigFile);
//        prepareWriteDataFile(records,kpiDestFile, ConfigFile);
//        if (transactionDir.equals("null") || transactionDestFile.equals("null")) {
//            System.out.println("Transaction Source or Dest File not found. Hence skipping Transaction data.");
//        } else{
//            List<String[]> transactionRecords = transactionData.generateBreakageDataTransaction(transactionDir,ConfigFile);
////            transactionRecords=filterDataonQueryTime(transactionRecords,ConfigFile, 0);
//            prepareWriteTransactionDataFile(transactionRecords,transactionDestFile);
//        }

//        if (kpiDir.equals("null") || kpiDestFile.equals("null")){
//            System.out.println("KPI Source or Dest File not found. Hence skipping KPI data.");
//        } else{
//            List<String[]> kpiRecords = kpiData.generateBreakageDataKpi(kpiDir, mstFile,ConfigFile);
////            kpiRecords=filterDataonQueryTime(kpiRecords,ConfigFile, 1);
//            preparewriteKpiDataFile(kpiRecords,kpiDestFile, ConfigFile);
//        }
        System.out.println("Exiting main()");
    }

    private static void prepareWriteDataFile(List<String[]> transactionRecords, String transactionDestFile, String ConfigFile) {
        System.out.println("Entering prepareWriteDataFile()");
        Path path = Paths.get(transactionDestFile);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        String[] reqdColumns=((String) ((JSONObject)jsonObject.get("query")).get("reqdColumns")).split(",");
        HashMap<Integer, String> hmap = new HashMap<Integer, String>();
        String a = "";
        for(int i =1;i<reqdColumns.length-1;i++){
            hmap.put(i,reqdColumns[i]);
        }
        Set set = hmap.entrySet();
        Iterator iterator = set.iterator();
        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();
            a+=mentry.getValue()+", ";
//            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
//            System.out.println(mentry.getValue());
        }

//        System.out.println(hmap.values().toString());
//        for (int i =0;i<hmap.size();i++){
//            System.out.println(hmap.keySet().toArray()[i]);
//        }
        appendUsingFileWriter(transactionDestFile, "DataBreakage(Minutes), "+a+"StartTime, EndTime\n");
        //System.exit(1);
        List<String[]> entries = new ArrayList<>();
        List<String[]> timeDiffList = new ArrayList<>();
        for (int i = 0; i < transactionRecords.size(); i++) {
            entries.add(transactionRecords.get(i));
            String tmp = "";
            if (i == transactionRecords.size()-1) {
                tmp = transactionRecords.get(i)[allData.locHeadReqForTime];
            } else {
                tmp = transactionRecords.get(i + 1)[allData.locHeadReqForTime];
            }
            if (!transactionRecords.get(i)[allData.locHeadReqForTime].equals(tmp) || i==transactionRecords.size()-1 ) {
                writeData(entries, timeDiffList, hmap);
                entries = new ArrayList<>();
            }
        }

        for (int i = 0; i < timeDiffList.size(); i++) {
            for (int j = 0; j < timeDiffList.get(i).length - 1; j++){
                appendUsingFileWriter(transactionDestFile, timeDiffList.get(i)[j] + ", ");
            }
            appendUsingFileWriter(transactionDestFile,timeDiffList.get(i)[timeDiffList.get(i).length - 1]+"\n");
        }


//        for (int i = 0; i < timeDiffList.size(); i++) {
//            for (int j = 0; j < timeDiffList.get(i).length; j++)
//            System.out.print(timeDiffList.get(i)[j]+", ");
//            System.out.println();
//        }
        System.out.println("Exiting prepareWriteDataFile()");
    }

    private static void writeData(List<String[]> transactionRecords, List<String[]> timeDiffList, HashMap<Integer, String> hmap) {
        System.out.println("Entering writeData()");
        //System.out.println(accRef+","+insRef+","+kpiRef);

        List<ArrayList<String>> withoutBreakagesorted = new ArrayList<>();
        for (int i = 0; i < transactionRecords.size() - 1; i++) {
            int diff = Integer.parseInt(transactionRecords.get(i + 1)[allData.timeLocation]) - Integer.parseInt(transactionRecords.get(i)[allData.timeLocation]);
            int diffInMinutes = diff / 60;
            long miliSec1 = Long.parseLong(transactionRecords.get(i)[allData.timeLocation] + "000");
            long miliSec2 = Long.parseLong(transactionRecords.get(i + 1)[allData.timeLocation] + "000");
            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
            Date result1 = new Date(miliSec1);
            Date result2 = new Date(miliSec2);
            String currDate = simple.format(result1);
            String nextDate = simple.format(result2);
            if (diff > 60) {
                ArrayList<String> sample = new ArrayList<>();
                sample.add(String.valueOf(--diffInMinutes));
                Set set = hmap.entrySet();
                Iterator iterator = set.iterator();
                while(iterator.hasNext()) {
                    Map.Entry mentry = (Map.Entry)iterator.next();
//            System.out.print("key is: "+ mentry.getKey() + " & Value is: ");
//            System.out.println(mentry.getValue());
                    sample.add(transactionRecords.get(i)[(int) mentry.getKey()]);
                }
                sample.add(currDate);
                sample.add(nextDate);
                withoutBreakagesorted.add(sample);
            }
        }
        if(withoutBreakagesorted.size()==0){
            return;
        }
//            for (int i =0;i<withoutBreakagesorted.size();i++){
//                System.out.println(Arrays.toString(withoutBreakagesorted.get(i)));
//            }
        ArrayList<String[]> nn = new ArrayList<>();
        for(int i =0;i<withoutBreakagesorted.size();i++){
            String[] arr = new String[withoutBreakagesorted.get(i).size()];
            for(int j =0;j<arr.length;j++){
                arr[j] = withoutBreakagesorted.get(i).get(j);
            }
            nn.add(arr);
        }
//        for(int i =0;i<nn.size();i++){
//            System.out.println(Arrays.toString(nn.get(i)));
//        }
        sortBreakageTimeStamp(0, withoutBreakagesorted.size()-1,nn);

        for (int i =  withoutBreakagesorted.size() -1; i >= 0; i--) {
            timeDiffList.add(nn.get(i));
        }
        System.out.println("Exiting writeData()");
    }


    private static void preparewriteKpiDataFile(List<String[]> kpiRecords, String kpiDestFile, String ConfigFile) {
        System.out.println("Entering preparewriteKpiDataFile()");
        Path path = Paths.get(kpiDestFile);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        appendUsingFileWriter(kpiDestFile, "DataBreakage(Minutes), Account, Instance, KPI, StartTime, EndTime\n");

        List<String[]> entries = new ArrayList<>();
        List<String[]> timeDiffList = new ArrayList<>();
        for (int i = 0; i < kpiRecords.size(); i++) {
            entries.add(kpiRecords.get(i));
            String tmp = "";
            if (i == kpiRecords.size()-1) {
                tmp = kpiRecords.get(i)[kpiData.kpi_idLocation];
            } else {
                tmp = kpiRecords.get(i + 1)[kpiData.kpi_idLocation];
            }
            if (!kpiRecords.get(i)[kpiData.kpi_idLocation].equals(tmp) || i==kpiRecords.size()-1 ) {
                writeDataKpi(entries, timeDiffList);
                entries = new ArrayList<>();
            }
        }

        for (int i = 0; i < timeDiffList.size(); i++) {
            for (int j = 0; j < timeDiffList.get(i).length - 1; j++){
                appendUsingFileWriter(kpiDestFile, timeDiffList.get(i)[j] + ", ");
            }
            appendUsingFileWriter(kpiDestFile,timeDiffList.get(i)[timeDiffList.get(i).length - 1]+"\n");
        }


//        for (int i = 0; i < timeDiffList.size(); i++) {
//            for (int j = 0; j < timeDiffList.get(i).length; j++)
//            System.out.print(timeDiffList.get(i)[j]+", ");
//            System.out.println();
//        }
        System.out.println("Exiting preparewriteKpiDataFile()");
    }

    private static void writeDataKpi(List<String[]> kpiRecords, List<String[]> timeDiffList) {
        System.out.println("Entering writeDataKpi()");
        ArrayList<String[]> withoutBreakagesorted = new ArrayList<String[]>();
        for (int i = 0; i < kpiRecords.size() - 1; i++) {
            int diff = Integer.parseInt(kpiRecords.get(i + 1)[kpiData.timeLocation]) - Integer.parseInt(kpiRecords.get(i)[kpiData.timeLocation]);
            int diffInMinutes = diff / 60;
            long miliSec1 = Long.parseLong(kpiRecords.get(i)[kpiData.timeLocation] + "000");
            long miliSec2 = Long.parseLong(kpiRecords.get(i + 1)[kpiData.timeLocation] + "000");
            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
            Date result1 = new Date(miliSec1);
            Date result2 = new Date(miliSec2);
            String currDate = simple.format(result1);
            String nextDate = simple.format(result2);
            if (diff > 60) {
                String[] temp = new String[]{String.valueOf(--diffInMinutes), kpiRecords.get(i)[kpiData.account_idLocation],
                        kpiRecords.get(i)[kpiData.instance_idLocation], kpiRecords.get(i)[kpiData.kpi_idLocation],
                        currDate, nextDate};
                withoutBreakagesorted.add(temp);
            }
        }
        if(withoutBreakagesorted.size()==0){
            return;
        }
        sortBreakageTimeStamp(0, withoutBreakagesorted.size()-1,withoutBreakagesorted);

        for (int i =  withoutBreakagesorted.size() -1; i >= 0; i--) {
            timeDiffList.add(withoutBreakagesorted.get(i));
        }
        System.out.println("Exiting writeDataKpi()");
    }

    private static void prepareWriteTransactionDataFile(List<String[]> transactionRecords, String transactionDestFile) {
        System.out.println("Entering prepareWriteTransactionDataFile()");
        Path path = Paths.get(transactionDestFile);
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        appendUsingFileWriter(transactionDestFile, "DataBreakage(Minutes), Account, Application, Service, StartTime, EndTime\n");

        List<String[]> entries = new ArrayList<>();
        List<String[]> timeDiffList = new ArrayList<>();
        for (int i = 0; i < transactionRecords.size(); i++) {
            entries.add(transactionRecords.get(i));
            String tmp = "";
            if (i == transactionRecords.size()-1) {
                tmp = transactionRecords.get(i)[transactionData.service_idLocation];
            } else {
                tmp = transactionRecords.get(i + 1)[transactionData.service_idLocation];
            }
            if (!transactionRecords.get(i)[transactionData.service_idLocation].equals(tmp) || i==transactionRecords.size()-1 ) {
                writeDataTransaction(entries, timeDiffList);
                entries = new ArrayList<>();
            }
        }

        for (int i = 0; i < timeDiffList.size(); i++) {
            for (int j = 0; j < timeDiffList.get(i).length - 1; j++){
                appendUsingFileWriter(transactionDestFile, timeDiffList.get(i)[j] + ", ");
            }
            appendUsingFileWriter(transactionDestFile,timeDiffList.get(i)[timeDiffList.get(i).length - 1]+"\n");
        }


//        for (int i = 0; i < timeDiffList.size(); i++) {
//            for (int j = 0; j < timeDiffList.get(i).length; j++)
//            System.out.print(timeDiffList.get(i)[j]+", ");
//            System.out.println();
//        }
        System.out.println("Exiting prepareWriteTransactionDataFile()");
    }

    private static void writeDataTransaction(List<String[]> transactionRecords, List<String[]> timeDiffList) {
        System.out.println("Entering writeDataTransaction()");
            //System.out.println(accRef+","+insRef+","+kpiRef);
            ArrayList<String[]> withoutBreakagesorted = new ArrayList<String[]>();
            for (int i = 0; i < transactionRecords.size() - 1; i++) {
                int diff = Integer.parseInt(transactionRecords.get(i + 1)[transactionData.timeLocation]) - Integer.parseInt(transactionRecords.get(i)[transactionData.timeLocation]);
                int diffInMinutes = diff / 60;
                long miliSec1 = Long.parseLong(transactionRecords.get(i)[transactionData.timeLocation] + "000");
                long miliSec2 = Long.parseLong(transactionRecords.get(i + 1)[transactionData.timeLocation] + "000");
                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
                Date result1 = new Date(miliSec1);
                Date result2 = new Date(miliSec2);
                String currDate = simple.format(result1);
                String nextDate = simple.format(result2);
                if (diff > 60) {
                    String[] temp = new String[]{String.valueOf(--diffInMinutes), transactionRecords.get(i)[transactionData.account_idLocation],
                    transactionRecords.get(i)[transactionData.application_idLocation], transactionRecords.get(i)[transactionData.service_idLocation],
                            currDate, nextDate};
                    withoutBreakagesorted.add(temp);
                }
            }
            if(withoutBreakagesorted.size()==0){
                return;
            }
//            for (int i =0;i<withoutBreakagesorted.size();i++){
//                System.out.println(Arrays.toString(withoutBreakagesorted.get(i)));
//            }
            sortBreakageTimeStamp(0, withoutBreakagesorted.size()-1,withoutBreakagesorted);

            for (int i =  withoutBreakagesorted.size() -1; i >= 0; i--) {
                timeDiffList.add(withoutBreakagesorted.get(i));
            }
        System.out.println("Exiting writeDataTransaction()");
        }

    private static void sortBreakageTimeStamp(int low, int high,ArrayList<String[]> withoutBreakagesorted) {
        System.out.println("Entering sortBreakageTimeStamp()");
        int pivot = Integer.parseInt(withoutBreakagesorted.get((low + high) / 2)[0]);
        int i = low, j = high;
        while (i <= j) {
            while (Integer.parseInt(withoutBreakagesorted.get(i)[0]) < pivot) i++;
            while (Integer.parseInt(withoutBreakagesorted.get(j)[0])> pivot)  j--;
            if (i <= j) {
                String[] temp = withoutBreakagesorted.get(i);
                withoutBreakagesorted.set(i, withoutBreakagesorted.get(j));
                withoutBreakagesorted.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortBreakageTimeStamp(low, j, withoutBreakagesorted);
        if (i < high) sortBreakageTimeStamp(i, high, withoutBreakagesorted);
        System.out.println("Exiting sortBreakageTimeStamp() ");
    }


    //    private static List<String[]> writeDataTransaction(List<String[]> transactionRecords, String transactionDestFile, List<String[]> references) {
//        String accRef = references.get(0)[0];
//        String appRef = references.get(0)[1];
//        String serRef = references.get(0)[2];
//        //System.out.println(accRef+","+insRef+","+kpiRef);
//        for (int i = 0; i < transactionRecords.size() - 1; i++) {
//            if(!(transactionRecords.get(i)[transactionData.account_idLocation]).equals(accRef)){
//                references.get(0)[0] = transactionRecords.get(i)[transactionData.account_idLocation];
//                accRef = references.get(0)[0];
//                appendUsingFileWriter(transactionDestFile, "\n"+accRef+" :-\n");
//            }
//            if(!(transactionRecords.get(i)[transactionData.application_idLocation]).equals(appRef)){
//                references.get(0)[1] = transactionRecords.get(i)[transactionData.application_idLocation];
//                appRef = references.get(0)[1];
//                appendUsingFileWriter(transactionDestFile, "\n\t"+appRef+" :-\n");
//            }
//            if(!(transactionRecords.get(i)[transactionData.service_idLocation]).equals(serRef)){
//                references.get(0)[2] = transactionRecords.get(i)[transactionData.service_idLocation];
//                serRef = references.get(0)[2];
//                appendUsingFileWriter(transactionDestFile, "\n\t\t"+serRef+" :-\n");
//            }
//            int diff = Integer.parseInt(transactionRecords.get(i + 1)[transactionData.timeLocation]) - Integer.parseInt(transactionRecords.get(i)[transactionData.timeLocation]);
//            int diffInMinutes = diff / 60;
//            long miliSec1 = Long.parseLong(transactionRecords.get(i)[transactionData.timeLocation] + "000");
//            long miliSec2 = Long.parseLong(transactionRecords.get(i + 1)[transactionData.timeLocation] + "000");
//            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
//            Date result1 = new Date(miliSec1);
//            Date result2 = new Date(miliSec2);
//            String currDate = simple.format(result1);
//            String nextDate = simple.format(result2);
//            if (diff > 60) {
//                String appendText = "\t\t\t"+ --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n";
//                appendUsingFileWriter(transactionDestFile, appendText);
//            }
//
//        }



//    private static void prepareWriteTransactionDataFile(List<String[]> transactionRecords, String transactionDestFile) {
//        Path path = Paths.get(transactionDestFile);
//        try {
//            Files.deleteIfExists(path);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        List<String []> references = new ArrayList<>();
//        appendUsingFileWriter(transactionDestFile, transactionRecords.get(0)[transactionData.account_idLocation]+" :-\n");
//        appendUsingFileWriter(transactionDestFile, "\n\t"+transactionRecords.get(0)[transactionData.application_idLocation]+" :-\n");
//        appendUsingFileWriter(transactionDestFile, "\n\t\t"+transactionRecords.get(0)[transactionData.service_idLocation]+" :-\n");
//
//        String accRef = transactionRecords.get(0)[transactionData.account_idLocation];
//        String appRef = transactionRecords.get(0)[transactionData.application_idLocation];
//        String serRef = transactionRecords.get(0)[transactionData.service_idLocation];
//
//        String[] a = new String[]{accRef,appRef,serRef};
//
//        references.add(0,a);
////        FileWriter fr1 = new FileWriter(kpiDestFile);
////        try {
////            fr1.write("account_id, instance_id, kpi_id, diffIn_minutes, Start Time, End Time\n");
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        }
////        fr1.close();
//        List<String[]> entries = new ArrayList<>();
//
//        for (int i = 0; i < transactionRecords.size(); i++) {
//            entries.add(transactionRecords.get(i));
//            String tmp = "";
//            if (i == transactionRecords.size()-1) {
//                tmp = transactionRecords.get(i)[transactionData.service_idLocation];
//            } else {
//                tmp = transactionRecords.get(i + 1)[transactionData.service_idLocation];
//            }
//            if (!transactionRecords.get(i)[transactionData.service_idLocation].equals(tmp) || i==transactionRecords.size()-1 ) {
//                references = writeDataTransaction(entries, transactionDestFile, references);
//                entries = new ArrayList<>();
//            }
//        }
//    }
//    private static List<String[]> writeDataTransaction(List<String[]> transactionRecords, String transactionDestFile, List<String[]> references) {
//        String accRef = references.get(0)[0];
//        String appRef = references.get(0)[1];
//        String serRef = references.get(0)[2];
//        //System.out.println(accRef+","+insRef+","+kpiRef);
//        for (int i = 0; i < transactionRecords.size() - 1; i++) {
//            if(!(transactionRecords.get(i)[transactionData.account_idLocation]).equals(accRef)){
//                references.get(0)[0] = transactionRecords.get(i)[transactionData.account_idLocation];
//                accRef = references.get(0)[0];
//                appendUsingFileWriter(transactionDestFile, "\n"+accRef+" :-\n");
//            }
//            if(!(transactionRecords.get(i)[transactionData.application_idLocation]).equals(appRef)){
//                references.get(0)[1] = transactionRecords.get(i)[transactionData.application_idLocation];
//                appRef = references.get(0)[1];
//                appendUsingFileWriter(transactionDestFile, "\n\t"+appRef+" :-\n");
//            }
//            if(!(transactionRecords.get(i)[transactionData.service_idLocation]).equals(serRef)){
//                references.get(0)[2] = transactionRecords.get(i)[transactionData.service_idLocation];
//                serRef = references.get(0)[2];
//                appendUsingFileWriter(transactionDestFile, "\n\t\t"+serRef+" :-\n");
//            }
//            int diff = Integer.parseInt(transactionRecords.get(i + 1)[transactionData.timeLocation]) - Integer.parseInt(transactionRecords.get(i)[transactionData.timeLocation]);
//            int diffInMinutes = diff / 60;
//            long miliSec1 = Long.parseLong(transactionRecords.get(i)[transactionData.timeLocation] + "000");
//            long miliSec2 = Long.parseLong(transactionRecords.get(i + 1)[transactionData.timeLocation] + "000");
//            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
//            Date result1 = new Date(miliSec1);
//            Date result2 = new Date(miliSec2);
//            String currDate = simple.format(result1);
//            String nextDate = simple.format(result2);
//            if (diff > 60) {
//                String appendText = "\t\t\t"+ --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n";
//                appendUsingFileWriter(transactionDestFile, appendText);
//            }
//
//        }
//
////        for (int i = 0; i < kpiRecords.size() - 1; i++) {
////                int diff = Integer.parseInt(kpiRecords.get(i + 1)[kpiData.timeLocation]) - Integer.parseInt(kpiRecords.get(i)[kpiData.timeLocation]);
////                int diffInMinutes = diff / 60;
////                long miliSec1 = Long.parseLong(kpiRecords.get(i)[kpiData.timeLocation] + "000");
////                long miliSec2 = Long.parseLong(kpiRecords.get(i + 1)[kpiData.timeLocation] + "000");
////                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
////                Date result1 = new Date(miliSec1);
////                Date result2 = new Date(miliSec2);
////                String currDate = simple.format(result1);
////                String nextDate = simple.format(result2);
////                if (diff > 60) {
////                    String appendText = (kpiRecords.get(i)[kpiData.account_idLocation] + ", " + kpiRecords.get(i)[kpiData.instance_idLocation] +", "+ kpiRecords.get(i)[kpiData.kpi_idLocation] + ", " + --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n");
////                    appendUsingFileWriter(kpiDestFile, appendText);
////                }
////            }
//
//        return references;
//    }
//
//
//
//    private static void preparewriteKpiDataFile(List<String[]> kpiRecords, String kpiDestFile) throws IOException {
//            Path path = Paths.get(kpiDestFile);
//            try {
//                Files.deleteIfExists(path);
//            } catch (IOException e) {
//            e.printStackTrace();
//        }
//            List<String []> references = new ArrayList<>();
//        appendUsingFileWriter(kpiDestFile, kpiRecords.get(0)[kpiData.account_idLocation]+" :-\n");
//        appendUsingFileWriter(kpiDestFile, "\n\t"+kpiRecords.get(0)[kpiData.instance_idLocation]+" :-\n");
//
//        String accRef = kpiRecords.get(0)[kpiData.account_idLocation];
//        String insRef = kpiRecords.get(0)[kpiData.instance_idLocation];
//        String kpiRef = kpiRecords.get(0)[kpiData.kpi_idLocation];
//
//        String[] a = new String[]{accRef,insRef,kpiRef};
//
//        references.add(0,a);
////        FileWriter fr1 = new FileWriter(kpiDestFile);
////        try {
////            fr1.write("account_id, instance_id, kpi_id, diffIn_minutes, Start Time, End Time\n");
////        } catch (IOException ex) {
////            ex.printStackTrace();
////        }
////        fr1.close();
//        List<String[]> entries = new ArrayList<>();
//
//        for (int i = 0; i < kpiRecords.size(); i++) {
//            entries.add(kpiRecords.get(i));
//            String tmp = "";
//            if (i == kpiRecords.size()-1) {
//                tmp = kpiRecords.get(i)[kpiData.kpi_idLocation];
//            } else {
//                tmp = kpiRecords.get(i + 1)[kpiData.kpi_idLocation];
//            }
//            if (!kpiRecords.get(i)[kpiData.kpi_idLocation].equals(tmp) || i==kpiRecords.size()-1 ) {
//                references = writeDataKpi(entries, kpiDestFile, references);
//                entries = new ArrayList<>();
//            }
//        }
//    }
//
//    private static List<String[]> writeDataKpi(List<String[]> kpiRecords, String kpiDestFile, List<String[]> references) {
//        String accRef = references.get(0)[0];
//        String insRef = references.get(0)[1];
//        String kpiRef = references.get(0)[2];
//        //System.out.println(accRef+","+insRef+","+kpiRef);
//        for (int i = 0; i < kpiRecords.size() - 1; i++) {
//            if(!(kpiRecords.get(i)[kpiData.account_idLocation]).equals(accRef)){
//                references.get(0)[0] = kpiRecords.get(i)[kpiData.account_idLocation];
//                accRef = references.get(0)[0];
//                appendUsingFileWriter(kpiDestFile, accRef+" :-\n");
//            }
//            if(!(kpiRecords.get(i)[kpiData.instance_idLocation]).equals(insRef)){
//                references.get(0)[1] = kpiRecords.get(i)[kpiData.instance_idLocation];
//                insRef = references.get(0)[1];
//                appendUsingFileWriter(kpiDestFile, "\n\n\t"+insRef+" :-");
//            }
//            if(!(kpiRecords.get(i)[kpiData.kpi_idLocation]).equals(kpiRef)){
//                references.get(0)[2] = kpiRecords.get(i)[kpiData.kpi_idLocation];
//                kpiRef = references.get(0)[2];
//                appendUsingFileWriter(kpiDestFile, "\n");
//            }
//            int diff = Integer.parseInt(kpiRecords.get(i + 1)[kpiData.timeLocation]) - Integer.parseInt(kpiRecords.get(i)[kpiData.timeLocation]);
//            int diffInMinutes = diff / 60;
//            long miliSec1 = Long.parseLong(kpiRecords.get(i)[kpiData.timeLocation] + "000");
//            long miliSec2 = Long.parseLong(kpiRecords.get(i + 1)[kpiData.timeLocation] + "000");
//            DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
//            Date result1 = new Date(miliSec1);
//            Date result2 = new Date(miliSec2);
//            String currDate = simple.format(result1);
//            String nextDate = simple.format(result2);
//            if (diff > 60) {
//                String appendText = "\t\t"+kpiRecords.get(i)[kpiData.kpi_idLocation] + ", " + --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n";
//                appendUsingFileWriter(kpiDestFile, appendText);
//            }
//
//        }
//
////        for (int i = 0; i < kpiRecords.size() - 1; i++) {
////                int diff = Integer.parseInt(kpiRecords.get(i + 1)[kpiData.timeLocation]) - Integer.parseInt(kpiRecords.get(i)[kpiData.timeLocation]);
////                int diffInMinutes = diff / 60;
////                long miliSec1 = Long.parseLong(kpiRecords.get(i)[kpiData.timeLocation] + "000");
////                long miliSec2 = Long.parseLong(kpiRecords.get(i + 1)[kpiData.timeLocation] + "000");
////                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
////                Date result1 = new Date(miliSec1);
////                Date result2 = new Date(miliSec2);
////                String currDate = simple.format(result1);
////                String nextDate = simple.format(result2);
////                if (diff > 60) {
////                    String appendText = (kpiRecords.get(i)[kpiData.account_idLocation] + ", " + kpiRecords.get(i)[kpiData.instance_idLocation] +", "+ kpiRecords.get(i)[kpiData.kpi_idLocation] + ", " + --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n");
////                    appendUsingFileWriter(kpiDestFile, appendText);
////                }
////            }
//
//        return references;
//    }

    private static void appendUsingFileWriter(String filePath, String text) {
        File file = new File(filePath);
        FileWriter fr = null;
        try {
            // Below constructor argument decides whether to append or override
            fr = new FileWriter(file, true);
            fr.write(text);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //    private static void prepareWriteTransactionDataFile(List<String[]> transactionRecords, String transactionDestFile) {
//        try {
//            FileWriter fr1 = new FileWriter(transactionDestFile);
//            try {
//                fr1.write("account_id, application_id, service_id, diffInMinutes, Start Time, End Time\n");
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//            for (int i = 0; i < transactionRecords.size() - 1; i++) {
//                int diff = Integer.parseInt(transactionRecords.get(i + 1)[transactionData.timeLocation]) - Integer.parseInt(transactionRecords.get(i)[transactionData.timeLocation]);
//                int diffInMinutes = diff / 60;
//                long miliSec1 = Long.parseLong(transactionRecords.get(i)[transactionData.timeLocation] + "000");
//                long miliSec2 = Long.parseLong(transactionRecords.get(i + 1)[transactionData.timeLocation] + "000");
//                DateFormat simple = new SimpleDateFormat("yyyy-MM-dd HH:mm:00");
//                Date result1 = new Date(miliSec1);
//                Date result2 = new Date(miliSec2);
//                String currDate = simple.format(result1);
//                String nextDate = simple.format(result2);
//                if (diff > 60) {
//                    fr1.write(transactionRecords.get(i)[transactionData.account_idLocation] + ", " + transactionRecords.get(i)[transactionData.application_idLocation] + ", "  + transactionRecords.get(i)[transactionData.service_idLocation] +", "+ --diffInMinutes + " mins, " + currDate + ", " + nextDate + "\n");
//                    //System.out.println(transactionRecords.get(i)[1] + ", " + transactionRecords.get(i)[2] + ", " + diffInMinutes + " mins, " + currDate + ", " + nextDate);
//                }
//
//            }
//            fr1.close();
//        } catch (
//                IOException e) {
//            e.printStackTrace();
//        }
//    }
}
