package com.project.appnomic;

import com.opencsv.CSVReader;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class allData {
    static int timeLocation = 0;
    static int account_idLocation = 0;
    static int agg_levelLocation = 0;
    static int locHeadReqForTime = 0;
    static int kpi_idLocation = 0;
    public static List<String[]> generateBreakageData(String directoryName, String ConfigFile, String mstFile, String kpi_idlocation) {
        System.out.println("Entering generateBreakageData()");
        kpi_idLocation = Integer.parseInt(kpi_idlocation);
        Reader reader = null;
        File directory = new File(directoryName);
        //get all the files from a directory
        File[] fList = directory.listFiles();
        List<String[]> records = new ArrayList<>();
        assert fList != null;
        for (File file : fList){
            System.out.println("Loading :-"+file);
            try {
                reader = Files.newBufferedReader(Paths.get(String.valueOf(file)));
            } catch (IOException e) {
                e.printStackTrace();
            }
            assert reader != null;
            CSVReader csvReader = new CSVReader(reader);
            List<String[]> individualRecords = null;
            try {
                individualRecords = csvReader.readAll();
                records.addAll(individualRecords);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < records.size(); i++) {
            for (int j = 0; j < records.get(i).length; j++) {
                records.get(i)[j] = records.get(i)[j].trim();
            }
        }
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }

        String[] reqdColumns=((String) ((JSONObject)jsonObject.get("query")).get("reqdColumns")).split(",");

        String timeRegex = reqdColumns[reqdColumns.length - 1];
        String account_idRegex = reqdColumns[1];
        String agg_levelRegex  = reqdColumns[0];
        String agg_lvl_value = (String) ((JSONObject)jsonObject.get("query")).get("aggregation_value");

        String kpi_idRegex = (String) ((JSONObject)jsonObject.get("mst_head")).get("kpi_id_header");
        String kpi_nameRegex = (String) ((JSONObject)jsonObject.get("mst_head")).get("kpi_name_header");
        for (int i = 0; i < records.get(0).length; i++) {
            if (records.get(0)[i].matches(timeRegex)) {
                timeLocation = i;
            }
            else if (records.get(0)[i].matches(account_idRegex)) {
                account_idLocation = i;
            }
            else if (records.get(0)[i].matches(agg_levelRegex)) {
                agg_levelLocation = i;
            }
        }

        List<String[]> recordsTemp = new ArrayList<>();
        int j =0;
        for (int i = 1; i < records.size(); i++) {
            if(records.get(i)[agg_levelLocation].equals(agg_lvl_value)){
                try{
                    recordsTemp.add(j,records.get(i));
                    j++;
                }
                catch(Exception e){
                    System.out.println(e);
                }
            }
        }
        records = recordsTemp;
        records.remove(0);

        records=filterDataonQueryTime(records,ConfigFile);
        sortAccount_id(0, records.size() - 1, records);
        int o= 0;
        for(o=1;o<reqdColumns.length-2;o++){
            records = preparesort_Id(records,o,o+1);
        }
        if(o == reqdColumns.length-2){
            records = preparesortTimeStamp(records,o);
        }
        if(kpi_idLocation>=0)
        changekpiName(records, mstFile, kpi_idRegex, kpi_nameRegex);
        return records;
    }
    private static List<String[]> filterDataonQueryTime(List<String[]> Records, String ConfigFile) {
        System.out.println("Entering filterDataonQueryTime()");
        List<String[]> RecordsNew=new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            FileReader fr = new FileReader(ConfigFile);
            jsonObject = (JSONObject) jsonParser.parse(fr);
            fr.close();
        } catch (org.json.simple.parser.ParseException | IOException e) {
            e.printStackTrace();
        }
        String startTime="";
        String endTime="";
        startTime = (String) ((JSONObject)jsonObject.get("query_time")).get("start_time");
        endTime = (String) ((JSONObject)jsonObject.get("query_time")).get("end_time");
        if(startTime.equals("") || endTime.equals("")){
            return Records;
        }
        long startT = dateToMS(startTime);
        long endT = dateToMS(endTime);
        if(startT>endT){
            return Records;
        }
        for(int i =0;i<Records.size();i++){

            if(dateToMS(Records.get(i)[allData.timeLocation].substring(0,19))>=startT && dateToMS(Records.get(i)[allData.timeLocation].substring(0,19))<=endT){
                RecordsNew.add(Records.get(i));
            }
        }
//            for(int i =0;i<RecordsNew.size();i++){
//                System.out.println(Arrays.toString(RecordsNew.get(i)));
//            }
        //msToDate(RecordsNew);
//        for(int i =0;i<RecordsNew.size();i++){
//            System.out.println(Arrays.toString(RecordsNew.get(i)));
//        }
        System.out.println("Exiting filterDataonQueryTime()");
        return RecordsNew;
    }
    private static long dateToMS(String time) {
        //System.out.println("Entering dateToMS()");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        long millis = date.getTime();
        //System.out.println("Exiting dateToMS()");
        return millis;
    }
    private static void sortAccount_id(int low, int high, List<String[]> records) {
        System.out.println("Entering sortAccount_id()");
        String pivot = records.get((low + high) / 2)[account_idLocation];
        int i = low, j = high;
        while (i <= j) {
            while (records.get(i)[account_idLocation].compareTo(pivot) < 0) i++;
            while (records.get(j)[account_idLocation].compareTo(pivot) > 0) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortAccount_id(low, j, records);
        if (i < high) sortAccount_id(i, high, records);
        System.out.println("Exiting sortAccount_id() ");
    }

    private static List<String[]> preparesort_Id(List<String[]> records, int headerPre, int headerCurr) {
        System.out.println("Entering preparesort_Id() ");
        List<String[]> entries = new ArrayList<>();
        List<String[]> final1 = new ArrayList<>();
        int mm = 0;
        //entries.add(items1);
        for (int i = 0; i < records.size(); i++) {
            entries.add(records.get(i));
            String tmp = "";
            if (i == records.size() - 1) {
                tmp = records.get(i)[headerPre];
            } else {
                tmp = records.get(i + 1)[headerPre];
            }
            if (!records.get(i)[headerPre].equals(tmp) || i == records.size() - 1) {

                List<String[]> entriesRet = sort_Id(0, entries.size() - 1, entries,headerCurr);
                for (int u = 0; u < entriesRet.size(); u++) {
                    final1.add(mm, entriesRet.get(u));
                    mm++;
                }
                entries = new ArrayList<>();
            }

        }
        System.out.println("Exiting preparesortApplication_Id() ");
        return final1;
    }

    private static List<String[]> sort_Id(int low, int high, List<String[]> records, int headerCurr) {
        System.out.println("Entering sort_Id() ");
        String pivot ="";
        try{
            pivot = records.get((low + high) / 2)[headerCurr];
        }catch (Exception e){
            System.out.println(e);
            System.out.println(Arrays.toString(records.get((low + high) / 2)));
        }

        int i = low, j = high;
        while (i <= j) {
            while (records.get(i)[headerCurr].compareTo(pivot) < 0) i++;
            while (records.get(j)[headerCurr].compareTo(pivot) > 0) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sort_Id(low, j, records,headerCurr);
        if (i < high) sort_Id(i, high, records,headerCurr);
        System.out.println("Exiting sort_Id() ");
        return records;
    }
    private static void printData(List<String[]> records, int a) {
        try {
            FileWriter fr = new FileWriter("C:\\Users\\HP\\Desktop\\transactionSorted"+a+".txt");
            for(int i = 1;i<records.size();i++){
                fr.write(i+"  "+ Arrays.toString(records.get(i)) +"\n");
                //System.out.println(i+"  "+records.get(i)[3]);
            }
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String[]> preparesortTimeStamp(List<String[]> records, int headerPre) {
        locHeadReqForTime = headerPre;
        System.out.println("Entering praparesortTimeStamp() ");
        List<String[]> entries = new ArrayList<>();
        List<String[]> final1 = new ArrayList<>();
        int mm = 0;
        //entries.add(items1);
        for (int i = 0; i < records.size(); i++) {
            entries.add(records.get(i));
            String tmp = "";
            if (i == records.size() - 1) {
                tmp = records.get(i)[headerPre];
            } else {
                tmp = records.get(i + 1)[headerPre];
            }
            if (!records.get(i)[headerPre].equals(tmp) || i == records.size() - 1) {

                List<String[]> entriesRet = srttimeStmp(0, entries.size() - 1, entries);
                for (int u = 0; u < entriesRet.size(); u++) {
                    final1.add(mm, entriesRet.get(u));
                    mm++;
                }
//                for(int u = 0;u<final1.size();u++){
//                    System.out.println(final1.get(u)[0]+","+final1.get(u)[1]+","+final1.get(u)[2]+","+final1.get(u)[3]);
//                }
//                System.out.println("----------------------------");
                entries = new ArrayList<>();
            }

        }
        //timeshort(final1);
//       for (int i =0;i<final1.size();i++) {
//          for(int j = 0;j<4;j++){
//              System.out.print(final1.get(i)[j]+",");
//           }
//           System.out.println();
//      }
//        System.out.println("=============++++++++++++++++++++++++++++++++===============================");
        System.out.println("Exiting praparesortTimeStamp() ");

        //printData(final1);
        return final1;
    }

    private static List<String[]> srttimeStmp(int low, int high, List<String[]> entries) {
        System.out.println("Entering srttimeStmp() ");
//        for (int i = 0; i < entries.size(); i++) {
//            for (int j = 0; j < entries.get(0).length; j++) {
//                System.out.println(entries.get(i)[j]+",");
//            }
//        }
        for (int i = 0; i < entries.size(); i++) {
            String a = "";
            try{
                a = entries.get(i)[timeLocation].substring(0, 19);
            }catch (Exception e){
                System.out.println(e);
                System.out.println( entries.get(i)[timeLocation]);
            }

            //2020-06-27 09:15:00.000000+0000
            //System.out.println(a);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = null;
            try {
                date = sdf.parse(a);
            } catch (ParseException ex) {
                ex.printStackTrace();
            }
            long millis = date.getTime();

            //System.out.println(millis);
            entries.get(i)[timeLocation] = String.valueOf(millis);
            entries.get(i)[timeLocation] = entries.get(i)[timeLocation].substring(0, entries.get(i)[timeLocation].length() - 3);
            //System.out.println(records.get(i)[0]);
//                for(int j = 0;j<4;j++){
//                    System.out.print(records.get(i)[j]+",");
//                }
//                System.out.println();
        }
        sortTimeStamp(0, entries.size() - 1, entries);
        System.out.println("Exiting srttimeStmp() ");
        return entries;
    }

    public static void sortTimeStamp(int low, int high, List<String[]> records) {
        System.out.println("Entering sortTimeStamp() ");
        int pivot = Integer.parseInt(records.get((low + high) / 2)[timeLocation]);
        int i = low, j = high;
        while (i <= j) {
            while (Integer.parseInt(records.get(i)[timeLocation]) < pivot) i++;
            while (Integer.parseInt(records.get(j)[timeLocation]) > pivot) j--;
            if (i <= j) {
                String[] temp = records.get(i);
                records.set(i, records.get(j));
                records.set(j, temp);
                i++;
                j--;
            }
        }
        if (low < j) sortTimeStamp(low, j, records);
        if (i < high) sortTimeStamp(i, high, records);
        System.out.println("Exiting sortTimeStamp() ");
    }
    private static void changekpiName(List<String[]> records, String mstFile, String kpi_idRegex, String kpi_nameRegex) {
        System.out.println("Entering changekpiName()");
        Reader reader = null;
        try {
            reader = Files.newBufferedReader(Paths.get(mstFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        CSVReader csvReader = new CSVReader(reader);
        List<String[]> recordsMst = null;
        try {
            recordsMst = csvReader.readAll();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int kpi_idLoc = 0;
        for (int i =0;i<recordsMst.get(0).length;i++) {
            if(recordsMst.get(0)[i].matches(kpi_idRegex)) {
                kpi_idLoc = i;
            }
        }
        int kpi_nameLoc = 0;
        for (int i =0;i<recordsMst.get(0).length;i++) {
            if(recordsMst.get(0)[i].matches(kpi_nameRegex)) {
                kpi_nameLoc = i;
            }
        }
        for( int i = 0; i < records.size();i++){
            for(int j = 0; j< recordsMst.size();j++){
                if(records.get(i)[kpi_idLocation].equals(recordsMst.get(j)[kpi_idLoc])){
                    records.get(i)[kpi_idLocation] = recordsMst.get(j)[kpi_nameLoc];
                }
            }
        }
        System.out.println("Exiting changekpiName()");
    }
}
