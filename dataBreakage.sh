#!/bin/bash

pathInDocker="/tmp/logs/"

pathOfCQL="/usr/bin/cqlsh"

limitFileSizeinLines="100000"
configFilePath="./MasterDir/config.json"
DirectoryinHost=$(cat $configFilePath | jq -r '.query.directoryPath')

mstFilePath="./MasterDir/mstDetails.txt"

javaFilePath="./MasterDir/DataBreakage.jar"
kpi_id_location=$(cat $configFilePath | jq -r '.query.kpi_id_location')

resultDirectoryPath="./Result/"
resultFileName=$(cat $configFilePath | jq -r '.query.resultFileName')
#echo $pathInDocker

cassandra_containerid=$(docker ps | grep cassandra | awk '{print $1}')

#kpiColumns=$(cat $configFilePath | jq -r '.query.kpiColumns')
table=$(cat $configFilePath | jq -r '.query.table')
reqdColumns=$(cat $configFilePath | jq -r '.query.reqdColumns')

commonFileName=$(cat $configFilePath | jq -r '.query.commonFileName')
#echo "$cassandra_containerid $kpiColumns $transactionColumns $commonFileName"

#myIP=$(hostname -I | awk '{print $1}')

myIP=$(cat $configFilePath | jq  '.percona_config.host_address' | sed "s/\"//g")

port=$(cat $configFilePath | jq  '.percona_config.port' | sed "s/\"//g")

username=$(cat $configFilePath | jq  '.percona_config.username' | sed "s/\"//g")

password=$(cat $configFilePath | jq  '.percona_config.password' | sed "s/\"//g")


#exit 0

mysql -h $myIP -P $port -u $username -p$password -t -e "use appsone; select * from mst_kpi_details;" | sed '/^+/d' | sed '/^\s*$/d; s/^|\s*//g; s/\s*|$//g; s/\s*|\s*/,/g' | awk -F, '{print $1,$2}' | sed 's/\([0-9]\)\s\([a-zA-Z]\)/\1,\2/g' | sed '1 s/\s/,/g' > $mstFilePath

if [ -d $DirectoryinHost ]
then 
	rm -r $DirectoryinHost
fi

docker exec -it $cassandra_containerid $pathOfCQL --ssl --no-color --execute "COPY appsone.$table($reqdColumns) TO '$pathInDocker$commonFileName' WITH HEADER = TRUE;" && docker cp $cassandra_containerid:$pathInDocker$commonFileName . && mkdir $DirectoryinHost

fileSizeinLines=$(wc -l $commonFileName | awk '{print $1}')

if [ "$fileSizeinLines" -gt $limitFileSizeinLines ]
	then
		split -l$limitFileSizeinLines $commonFileName $DirectoryinHost/rawD.
	else
		mv $commonFileName $DirectoryinHost/rawData.csv
fi


if [ -d $resultDirectoryPath ]
then
        rm -r $resultDirectoryPath
fi

mkdir $resultDirectoryPath

java -jar $javaFilePath $DirectoryinHost $mstFilePath $configFilePath $resultDirectoryPath/$resultFileName $kpi_id_location

rm $commonFileName
